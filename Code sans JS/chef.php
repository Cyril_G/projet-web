<?php
require("php/repeat/chef_header.php");
?>
<!-- Contents-->
<div class="container-fluid">
    <div class = "row">
        <div class="col-md-2 col-md-offset-1">

            <div class = "row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des membres</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class = 'text-center'>Nom</th>

                                    <th class = 'text-center'>Date</th>
                                </tr>
                            </thead>
                            <?php
                            if (isset($_SESSION['teamid'])) {
                                $str = "SELECT user_name,user_time FROM user WHERE (user_type = 1) AND (user_equipe_id = " . $_SESSION['teamid'] . ") ORDER BY user_time DESC";
                                $result = bdd($str);
                                if ($result) {
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_array()) {
                                            echo "<tbody>";
                                            echo "<tr>";
                                            echo "<th class = 'text-center'>";
                                            echo $row["user_name"];
                                            echo "</th>";
                                            echo "<th class = 'text-center'>";
                                            echo $row["user_time"];
                                            echo "</th>";
                                            echo "</tr>";
                                            echo "</tbody>";
                                        }
                                    }
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>    
            </div>


        </div>
        <div class="col-md-8">
            <div class = "row">
                <div class="tabs">
                    <!-- Radio button and lable for #tab-Reunion -->
                    <input type="radio" name="tabs" id="tab1" checked >
                    <label for="tab1">
                        <i class="fa fa-calendar"></i><span>Réunion</span>
                    </label>

                    <!-- Radio button and lable for #tab-Compte rendu -->
                    <input type="radio" name="tabs" id="tab2">
                    <label for="tab2">
                        <i class="fa fa-file-text"></i><span>Compte rendu</span>
                    </label> 

                    <!-- Radio button and lable for #tab-Demande -->
                    <input type="radio" name="tabs" id="tab3">
                    <label for="tab3">
                        <i class="fa fa-paper-plane"></i><span>Demande&nbsp <span class="badge">
                                <?php
                                echo $_SESSION['demande'];
                                ?>

                            </span></span>
                    </label> 
                    <!-- #tab-Reunion -->
                    <div id="tab-content1" class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="reunions">
                            <div class="panel panel-info">
                                <div class="panel-heading">Réunions</div>
                                <div class="panel-body embed-responsive-item"> 
                                    <div class="container-fluid"> 
                                        <form action ="chef.php" method="post">
                                            <div class = "row">
                                                <div class = "col-md-6">                                                                           	 
                                                    <div class = "row">
                                                        <div class ="col-md-3 text-center">
                                                            <h5>Titre</h5>
                                                        </div>
                                                        <div class = "col-md-9">                                                                                                           	
                                                            <input name="title" type="text" class="form-control" placeholder="Titre"> 
                                                        </div> 
                                                    </div>

                                                </div>
                                                <div class = "col-md-4">                                                                           	 
                                                    <div class = "row">
                                                        <div class ="col-md-3 text-center">
                                                            <h5>Type</h5>
                                                        </div>
                                                        <div class = "col-md-9">                                                                                                           	
                                                            <select name = "type">
                                                                <option  value="warning">Importante</option>
                                                                <option  value="info">Information</option>
                                                                <option  value="danger">Urgent</option>
                                                            </select>
                                                        </div> 
                                                    </div>

                                                </div>
                                                <div class = "col-md-2">
                                                    <input type="Submit" class="btn btn-primary" name="newevent" value="Créer"/>
                                                </div> 

                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class = "col-md-2 text-center">
                                                    <h5>Date de début</h5>
                                                </div>
                                                <div class = "col-md-3">
                                                    <?php
                                                    echo "<input name='bg-date' type='text' class='form-control' placeholder='" . date("Y-m-d") . "'>";
                                                    ?>

                                                </div>
                                                <div class = "col-md-2 text-center">
                                                    <h5>Heure</h5>
                                                </div>
                                                <div class = "col-md-3">
                                                    <?php
                                                    echo "<input name='bg-time' type='text' class='form-control' placeholder='" . date("H:i") . "'>";
                                                    ?>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class = "col-md-2 text-center">
                                                    <h5>Date de fin</h5>
                                                </div>
                                                <div class = "col-md-3">
                                                    <?php
                                                    echo "<input name='end-date' type='text' class='form-control' placeholder='" . date("Y-m-d") . "'>";
                                                    ?> 
                                                </div>
                                                <div class = "col-md-2 text-center">
                                                    <h5>Heure</h5>
                                                </div>
                                                <div class = "col-md-3">
                                                    <?php
                                                    echo "<input name='end-time' type='text' class='form-control' placeholder='" . date("H:i") . "'>";
                                                    ?>
                                                </div>
                                            </div>
                                        </form> 
                                        <?php
                                        if (@$_REQUEST['newevent']) {
                                            if (isset($_SESSION['teamid'])) {

                                                function test_input($data) {
                                                    $data = trim($data);
                                                    $data = stripslashes($data);
                                                    $data = htmlspecialchars($data);
                                                    return $data;
                                                }

                                                $event_title = test_input($_POST['title']);
                                                $event_type = $_POST['type'];
                                                $event_bgtime = $_POST['bg-date'] . " " . $_POST['bg-time'];
                                                $event_endtime = $_POST['end-date'] . " " . $_POST['end-time'];
                                                $teamid = $_SESSION['teamid'];
                                                if ($event_title != "") {
                                                    $event_bgtime = strtotime($event_bgtime) ? date("Y-m-d H:i", strtotime($event_bgtime)) : false;
                                                    $event_endtime = strtotime($event_endtime) ? date("Y-m-d H:i", strtotime($event_endtime)) : false;


                                                    if ($event_bgtime !== false && $event_endtime !== false) {


                                                        $str = "INSERT INTO meeting(meeting_title,meeting_type,meeting_begin,meeting_finish,meeting_team_id) VALUES ('" . $event_title . "','" . $event_type . "','" . $event_bgtime . "','" . $event_endtime . "'," . $teamid . ")";
                                                        $result = bdd($str);
                                                        if ($result) {
                                                            echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Réunion créée</div>";
                                                        } else {
                                                            echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Le format de la date ou de l'horaire n'est pas correct</div>";
                                                        }
                                                    } else {
                                                        echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Le format de la date ou de l'horaire n'est pas correct</div>";
                                                    }
                                                } else {
                                                    echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Le champ titre ne peut rester vide</div>";
                                                }
                                            } else {
                                                echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Vous ne faite pas parti du'une équipe</div>";
                                            }
                                        }
                                        ?>                                       
                                        <hr>
                                        <?php
                                        if (@$_REQUEST['event_delete']) {
                                            $eventid = $_POST['event_delete'];
                                            $str = "DELETE FROM meeting WHERE meeting_id =" . $eventid;
                                            bdd($str);
                                            echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>La réunion a bien était supprimé</div>";
                                        }
                                        ?>
                                        <div class="row"> 
                                            <form action ="chef.php" method="post">
                                                <table class="table table-hover">

                                                    <thead>
                                                        <tr>
                                                            <th> # </th>
                                                            <th>Titre</th>                                                   
                                                            <th>Début</th>
                                                            <th>Jusqu'à</th>
                                                            <th>Opération</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    if (isset($_SESSION['teamid'])) {
                                                        $teamid = $_SESSION['teamid'];
                                                        $str = "SELECT * FROM meeting WHERE (meeting_finish BETWEEN NOW() AND '2200-1-1 1:1') AND meeting_team_id =" . $teamid;
                                                        $result = bdd($str);
                                                        if ($result) {
                                                            if ($result->num_rows > 0) {
                                                                foreach ($result as $row) {
                                                                    echo "<tbody>";
                                                                    echo "<tr class ='" . $row['meeting_type'] . "'>";
                                                                    echo "<th>";
                                                                    echo $row["meeting_id"];
                                                                    echo "</th>";
                                                                    echo "<th>";
                                                                    echo $row["meeting_title"];
                                                                    echo "</th>";
                                                                    echo "<th>";
                                                                    echo $row["meeting_begin"];
                                                                    echo "</th>";
                                                                    echo "<th>";
                                                                    echo $row["meeting_finish"];
                                                                    echo "</th>";
                                                                    echo "<th>";
                                                                    echo "<button class = 'btn btn-danger' type='Submit' name='event_delete' value='" . $row["meeting_id"] . "'>Supprimer</button>";
                                                                    echo "</th>";
                                                                    echo "</tbody>";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </table>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- #tab-Compte rendu -->
                    <div id="tab-content2" class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="file">
                            <div class="panel panel-info">
                                <div class="panel-heading">Compte rendu</div>
                                <div class="panel-body embed-responsive-item">
                                    <div class ="container-fluid ">
                                        <div class ="row">
                                            <form action ="php/fileupload.php" method="post" enctype="multipart/form-data">
                                                <div class ="col-md-2 col-md-offset-2">
                                                    <label for="file">Fichier:</label>
                                                </div>
                                                <div class ="col-md-4 ">
                                                    <input  type="file" name="file" id="file" /> 
                                                </div>
                                                <div class ="col-md-2 ">
                                                    <input class = "btn btn-primary" type="submit" name="upload" value="Déposer" />
                                                </div>
                                            </form>
                                        </div>
                                        <hr>
                                        <div class="row">
                                         
                                            <form action='php/filedelete.php' method='post'>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class = 'text-center'>Nom du fichier</th>
                                                            <th class = 'text-center'>Opération</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    if (isset($_SESSION['teamid'])) {
                                                        $str = "SELECT * FROM report WHERE report_team_id =" . $_SESSION['teamid'];
                                                        $result = bdd($str);
                                                        if ($result->num_rows > 0) {
                                                            foreach ($result as $row) {
                                                                echo "<tbody>";
                                                                echo "<tr>";
                                                                echo "<th class = 'text-center'>";
                                                                $filename = explode('/', $row["report_con"]);
																
                                                                echo "<a href = '" . $row["report_con"] . "' target = '_blank'> " . $filename[2] . "</a>";
                                                                echo "</th>";
                                                                echo "<th class = 'text-center'>";
                                                                echo "<button class = 'fa fa-trash' type='Submit' name='de_file' value='".$row['report_con']."|".$row['report_id']."'></button>&nbsp";
                                                                 echo "</th>";
                                                                echo "</tr>";
                                                                echo "</tbody>";
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                </table>
                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- #tab-Demandes -->
                    <div id="tab-content3" class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="demande">
                            <div class="panel panel-info">
                                <div class="panel-heading">Demandes</div>
                                <div class="panel-body embed-responsive-item">
                                    <form action='php/demande.php' method='post'>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th> # </th>
                                                    <th>ID du demandeur</th>
                                                    <th>Nom du demandeur</th>
                                                    <th>Opération</th>
                                                </tr>
                                            </thead>

                                            <?php
                                            if ($_SESSION['demande'] > 0) {
                                                foreach ($_SESSION['demandes'] as $value) {
                                                    echo "<tbody>";
                                                    echo "<tr>";
                                                    echo "<th>";
                                                    echo $value["application_id"];
                                                    echo "</th>";
                                                    echo "<th>";
                                                    echo $value["application_user_id"];
                                                    echo "</th>";
                                                    echo "<th>";
                                                    echo $value["user_name"];
                                                    echo "</th>";
                                                    echo "<th>";
                                                    echo "<button class = 'btn btn-success' type='Submit' name='accept' value='" . $value["application_id"] . "-" . $value["application_user_id"] . "'>Accepter</button>" .
                                                    "&nbsp;&nbsp;&nbsp;&nbsp;<button class = 'btn btn-danger' type='Submit' name='refuse' value='" . $value["application_id"] . "'>Refuser</button>";
                                                    echo "</th>";
                                                    echo "</tr>";
                                                    echo "</tbody>";
                                                }
                                            }
                                            ?>
                                        </table>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

    </div>

    <hr>

</div>
<?php
require("php/repeat/footer.php");
?>

