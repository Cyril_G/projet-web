<?php
require('php/repeat/chef_header.php');
?>
<div class="container-fluid">
    <div class = "row">
        <div class="col-md-3"> </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">Paramètre</div>
                <div class="panel-body"> 
                    <div class="container-fluid">
                        <form action = "chefsetting.php" method="post">
                            <br/>
                            <div class ="row">
                                <div class="col-md-3"> 
                                    Ancien mot de passe
                                </div>
                                <div class="col-md-6"> 
                                    <input type="password" name="original_password" class="form-control"  placeholder="Ancien mot de passe">
                                </div>
                            </div>
                            <hr>
                            <div class ="row">
                                <div class="col-md-3"> 
                                    Nouveau mot de passe
                                </div>
                                <div class="col-md-6"> 
                                    <input type="password" name="new_password1" class="form-control"  placeholder="Nouveau mot de passe">
                                </div>
                            </div>
                            <br/>
                            <div class ="row">
                                <div class="col-md-3"> 
                                    Vérifier mot de passe
                                </div>
                                <div class="col-md-6"> 
                                    <input type="password" name="new_password2" class="form-control"  placeholder="Vérifier mot de passe">
                                </div>
                            </div>
                            <hr>
                            <div class ="row text-center">
                                <input class="btn btn-primary " type = "Submit" name ="valider" value ="Valider"/>
                            </div>
                        </form>
                        <?php
                        if (@$_REQUEST['valider']) {
                              function test_input($data) {
                            $data = trim($data);
                            $data = stripslashes($data);
                            $data = htmlspecialchars($data);
                            return $data;
                        }

                        
                            $og_pass = test_input($_POST['original_password']);
                            $new_pass1 = test_input($_POST['new_password1']);
                            $new_pass2 = test_input($_POST['new_password2']);
                            
                            if ($og_pass == "" || $new_pass1 == "" || $new_pass2 == "") {
                                echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong> Le mot de passe ne peut être vide</div>";
                            } else if ($new_pass1 != $new_pass2) {
                                echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong> Les 2 mot de passe doivent être identiques</div>";
                            } else {
                                
                                $userid = $_SESSION["userid"];
                                $og_pass = sha1($og_pass);
                                $str = "SELECT user_id FROM user WHERE user_id =" . $userid . " AND user_motdepasse = '" . $og_pass . "'";
                                $result = bdd($str);
                                if ($result->num_rows > 0) {
                                    $new_pass2 = sha1($new_pass2);
                                    $str = "UPDATE user set user_motdepasse='" . $new_pass2 . "' WHERE user_id=" . $userid;
                                    bdd($str);
                                    echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Le mot de passe a bien était changé</div>";
                                } else {
                                    echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong> Mot de passe incorrect</div>";
                                }
                            }
                        }
                        ?>
                    </div>        
                </div>
            </div>
        </div>
        <div class="col-md-3"> </div>
    </div>
</div>
<?php
require('php/repeat/footer.php');
?>