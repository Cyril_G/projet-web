<?php
require("php/repeat/chef_header.php");
?>
<!-- Contents-->
<div class="container">
    <div class="row text-center">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Création de l'équipe</h3>
            </div>
            <div class="panel-body">
                <div calss = "container">
                    <form action = "creatteam.php" method = "post">
                        <div class = "row">
                            <div class=" text-center col-lg-4 ">
                                <h5>Nom d'équipe</h5>
                            </div>
                            <div class=" text-center col-lg-4 ">
                                <input type="text" name="teamname"  class="form-control" placeholder="Nom d'équipe">
                            </div>
                        </div>
                        <hr>
                        <div class = "row">
                            <div class=" text-center col-lg-4 ">
                                <h5>Description</h5>
                            </div>
                            <div class=" text-center col-lg-4 ">
                                <textarea name="teamdiscription" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div id = "error_div" class = "text-center"> </div>
                        <hr>
                        <div class = "row">
                            <div class=" text-center">
                                <button type="Submit" class="btn btn-primary " >Valider</button>
                            </div>
                        </div>
                    </form>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {

                        function test_input($data) {
                            $data = trim($data);
                            $data = stripslashes($data);
                            $data = htmlspecialchars($data);
                            return $data;
                        }

                        $name_team = test_input(@$_POST["teamname"]);
                        $discription_team = test_input(@$_POST["teamdiscription"]);
                        $teamid = $_SESSION["teamid"];

                        if ($name_team != "" && $discription_team != "") {
                            if ($teamid == null) {   // one chef only have one team
                                
                                $str = "INSERT INTO team(team_name,team_discription,team_chef_id) VALUES('" . $name_team . "','" . $discription_team . "'," . $_SESSION["userid"] . ")";
                                bdd($str);
                                $str = "SELECT team_id FROM team WHERE team_chef_id =" . $_SESSION["userid"];
                                $result = bdd($str);
                                while ($row = $result->fetch_array()) {
                                    $teamid = $row["team_id"];
                                }
                                $_SESSION["teamid"] = $teamid;
                                $str = "UPDATE user SET user_equipe_id = " . $teamid . " WHERE user_id =" . $_SESSION["userid"];
                                $result = bdd($str);
                                if ($result) {
                                    echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button> L'équipe a bien était créée</div>";
                                }
                            } else
                                echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Chaque chef ne peut avoir qu'une seul équipe</div>";
                        } else
                            echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong> Les champs ne doivent pas rester vide</div>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
require("php/repeat/footer.php");
?>

