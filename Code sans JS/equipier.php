<?php
require("php/repeat/equipier_header.php");
?>

<!-- Contents-->
<div class="container-fluid">
    <div class = "row">
        <div class ="col-md-2 col-md-offset-1">

            <div class = "row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des membres</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class = 'text-center'>Nom</th>

                                    <th class = 'text-center'>Date</th>
                                </tr>
                            </thead>
                            <?php
                            if (isset($_SESSION['teamid'])) {
                                $str = "SELECT user_name,user_time FROM user WHERE (user_type = 1) AND (user_equipe_id = " . $_SESSION['teamid'] . ") ORDER BY user_time DESC";
                                $result = bdd($str);
                                if ($result) {
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_array()) {
                                            echo "<tbody>";
                                            echo "<tr>";
                                            echo "<th class = 'text-center'>";
                                            echo $row["user_name"];
                                            echo "</th>";
                                            echo "<th class = 'text-center'>";
                                            echo $row["user_time"];
                                            echo "</th>";
                                            echo "</tr>";
                                            echo "</tbody>";
                                        }
                                    }
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>    
            </div>


        </div>
        <div class="col-md-8">

            <div class = "row">
                <div class="tabs">
                    <!-- Radio button and lable for #tab-content1 -->
                    <input type="radio" name="tabs" id="tab1" checked >
                    <label for="tab1">
                        <i class="fa fa-calendar"></i><span>Réunion</span>
                    </label>

                    <!-- Radio button and lable for #tab-content2 -->
                    <input type="radio" name="tabs" id="tab2">
                    <label for="tab2">
                        <i class="fa fa-file-text"></i><span>Compte rendu</span>
                    </label> 
                    <!-- #tab-Reunion -->

                    <div id="tab-content1" class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="reunions">
                            <div class="panel panel-info">
                                <div class="panel-heading">Réunions</div>
                                <div class="panel-body embed-responsive-item">
                                    <div calss ="container-fluid">

                                        <div class="row"> 


                                            <table class="table table-hover">

                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Titre</th>                                                   
                                                        <th>Début</th>
                                                        <th>Jusqu'à</th>
                                                    </tr>
                                                </thead>
                                                <?php
                                                if (isset($_SESSION['teamid'])) {
                                                    $teamid = $_SESSION['teamid'];
                                                    $str = "SELECT * FROM meeting WHERE (meeting_finish BETWEEN NOW() AND '2200-1-1 1:1') AND meeting_team_id =" . $teamid;
                                                    $result = bdd($str);
                                                    if ($result) {
                                                        if ($result->num_rows > 0) {
                                                            foreach ($result as $row) {
                                                                echo "<tbody>";
                                                                echo "<tr class ='" . $row['meeting_type'] . "'>";
                                                                echo "<th>";
                                                                echo $row["meeting_id"];
                                                                echo "</th>";
                                                                echo "<th>";
                                                                echo $row["meeting_title"];
                                                                echo "</th>";
                                                                echo "<th>";
                                                                echo $row["meeting_begin"];
                                                                echo "</th>";
                                                                echo "<th>";
                                                                echo $row["meeting_finish"];
                                                                echo "</th>";

                                                                echo "</tbody>";
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- #tab-Compte rendu -->
                    <div id="tab-content2" class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="reunions">
                            <div class="panel panel-info">
                                <div class="panel-heading">Compte rendu</div>
                                <div class="panel-body embed-responsive-item">   
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class = 'text-center'>Nom du fichier</th>

                                            </tr>
                                        </thead>
                                        <?php
                                        if (isset($_SESSION['teamid'])) {
                                            $str = "SELECT * FROM report WHERE report_team_id =" . $_SESSION['teamid'];
                                            $result = bdd($str);
                                            if ($result->num_rows > 0) {
                                                foreach ($result as $row) {
                                                    echo "<tbody>";
                                                    echo "<tr>";
                                                    echo "<th class = 'text-center'>";
                                                    $filename = explode('/', $row["report_con"]);
                                                    echo "<a href = '" . $row["report_con"] . "' target = '_blank'> " . $filename[2] . "</a>";
                                                    echo "</th>";
                                                    echo "</tr>";
                                                    echo "</tbody>";
                                                }
                                            }
                                        }
                                        ?>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

    </div>

    <hr>

</div>
<?php
require("php/repeat/footer.php");
?>