<?php
require("php/repeat/equipier_header.php");
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1 class="text-center" id = "name_team">Kaggle ESIGELEC</h1>
        </div>
    </div>
    <hr>
</div>
<!-- Contents-->
<div class="container">
    <table class="table table-hover">
        <thead>
            <tr>
                <th> # </th>
                <th>Nom de l'équipe</th>
                <th>Chef d'équipe</th>
                <th>Description</th>
                <th>Date de création</th>
                <th>S'inscrire</th>
            </tr>
        </thead>
        <form action = "jointeam.php" method = "post">
            <?php
            $str = "SELECT team.team_id,team.team_name,user.user_name,team.team_discription,team.team_time FROM team INNER JOIN user ON team.team_chef_id=user.user_id ";
            $result = bdd($str);
            if ($result) {
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_array()) {
                        echo "<tbody>";
                        echo "<tr>";
                        echo "<th>";
                        echo  $row["team_id"];
                        echo "</th>";
                        echo "<th>";
                        echo $row["team_name"];
                        echo "</th>";
                        echo "<th>";
                        echo $row["user_name"];
                        echo "</th>";
                        echo "<th>";
                        echo $row["team_discription"];
                        echo "</th>";
                        echo "<th>";
                        echo $row["team_time"];
                        echo "</th>";
                        echo "<th>";
                        echo "<button type='Submit' class='btn btn-primary btn-block' name='join' value ='".$row["team_id"]."'>Rejoindre</button>";
                        echo "</th>";
                        echo "</tr>";
                        echo "</tbody>";
                    }
                }
            }
            ?>


    </table>
</form>

<?php
if (@$_REQUEST['join']) {
    if (@$_SESSION['teamid'] == NULL) {

        $str = "SELECT application_id FROM application WHERE application_user_id =" . $_SESSION['userid'];
        $result = bdd($str);
        if ($result->num_rows > 0) {
             echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Votre demande a bien était envoyé, veuillez patienter le temps que le chef d'équipe réponde à votre demande </div>";
                                       
            
        }
        else{
            $str = "INSERT INTO application(application_user_id,application_team_id) VALUES(" . $_SESSION['userid'] . "," . $_POST["join"] . ")";
            bdd($str);
         echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Votre demande a bien était envoyé, veuillez patienter le temps que le chef d'équipe réponde à votre demande </div>";
             
                 
        }
        
    } else{
        echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Une seule personne ne peut être le chef que d'une équipe</div>";
         
         
    }
}
?>
</div>



<hr>

<?php
require("php/repeat/footer.php");
?>