<?php

session_start();

require('bdd.php');
if (@$_REQUEST['accept']) {
    $receive = $_POST['accept'];
    $arr = explode("-",$receive);
    
    $str = "UPDATE user SET user_equipe_id=" . $_SESSION['teamid'] . " WHERE user_id=" . $arr[1];
    bdd($str);
    $str = "DELETE FROM application WHERE application_id =" . $arr[0];
    bdd($str);

    $count = 0;
    foreach ($_SESSION['demandes'] as $value) {
        if ($value['application_id'] == $arr[0]) {
            array_splice($_SESSION['demandes'], $count, 1);
            $_SESSION['demande'] -= 1;
            break;
        }
        $count++;
    }
    header("Location: ../chef.php");
    exit;
} else if (@$_REQUEST['refuse']) {
    $appid = $_POST['refuse'];
    $str = "DELETE FROM application WHERE application_id =" . $appid;
    bdd($str);
    $count = 0;
    foreach ($_SESSION['demandes'] as $value) {
        if ($value['application_id'] == $appid) {
            array_splice($_SESSION['demandes'], $count, 1);
            $_SESSION['demande'] -= 1;
            break;
        }
        $count++;
    }
    header("Location: ../chef.php");
    exit;
}
?>