<?php
session_start(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Kaggle-ESIGELEC</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/chef.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php 
require("php/bdd.php");
////////////////////////////////////////
//  make sure if it is legal login
////////////////////////////////////////
	if( @$_SESSION["admin"]  && @isset($_SESSION["code"])){  
		if(@$_SESSION["type"] != 2){
		//header("Location: ../php/exit.php");   // ilegal login 
		session_destroy();
		echo "Illégal connection";
		echo "<a href = 'index.php'>Page d'accueil</a>";
		exit;
		}
	}
	else{ 
	session_destroy();
	echo "Illégal connection";
		echo "<a href = 'index.php'>Page d'acceuil</a>";   
		exit;		
	}				
?>
<nav class="navbar navbar-default" style="background-color:red">
  <div class="container-fluid"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="chef.php">Kaggle</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav navbar-right ">
        <li ><a href="chef.php"><i class="fa fa-user"></i>&nbsp&nbsp;
        <?php
        echo @$_SESSION['username'];
		?></a></li>		
            <li><a href="chefsetting.php"><i class="fa fa-cog"></i>&nbsp&nbsp;Parametre</a></li>          
            <li><a href="creatteam.php"><i class="fa fa-trophy"></i>&nbsp&nbsp;Creer une equipe</a></li>
            <li><a href="php/exit.php"><i class="fa fa-power-off"></i>&nbsp&nbsp;Deconnection</a></li>                 
      </ul>
    </div>
 
  </div>
</nav>