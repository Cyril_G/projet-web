<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kaggle-ESIGELEC</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" id="header">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-center">KAGGLE ESIGELEC</h1>
                </div>
            </div>
            <hr>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 text-center col-lg-4 col-lg-offset-2"> <img  src="img/champion.jpg" alt= "logo" class="img-responsive">
                    <a href = "listequipe.php" class = "btn btn-primary btn-lg btn-block">Liste de équipes </a>
                </div>
                <div class="text-center col-sm-4 col-lg-offset-1">
                    <div class="tabs">
                        <!-- Radio button and lable for #tab-content1 -->
                        <input type="radio" name="tabs" id="tab1" checked >
                        <label for="tab1">
                            <i class="fa fa-sign-in"></i><span>Connecter</span>
                        </label>
                        <!-- Radio button and lable for #tab-content2 -->
                        <input type="radio" name="tabs" id="tab2">
                        <label for="tab2">
                            <i class="fa fa-user-plus"></i><span>Inscription</span>
                        </label>
                        <div id="tab-content1" class="tab-content">
                            <!-- Connection -->         
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3> Connection </h3>
                                </div>
                                <div class="panel-body">
                                    <form action="index.php" method = "post">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5>Identifiant</h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="email_div">
                                                    <input type="email" name = "email_login" class="form-control" placeholder="Email">
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Mot de passe </h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="password_div">
                                                    <input type="password" name="password_login" class="form-control"  placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="row" id="error_login"> </div>
                                            <hr>
                                            <div class="row">
                                                <div class=" text-center"> <a> Mot de passe oublié?</a> </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center">
                                                    <input class="btn btn-primary " type = "Submit" name ="connecter" value ="Connecter"/>
                                                </div>
                                            </div>     
                                        </div>
                                    </form>
                                    <?php
                                    if (@$_REQUEST['connecter']) {
                                        $email = $password = $result = $username = $limit = $userequipe = $userid = "";

                                        function test_input($data) {
                                            $data = trim($data);
                                            $data = stripslashes($data);
                                            $data = htmlspecialchars($data);
                                            return $data;
                                        }

                                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                            $email = test_input(@$_POST["email_login"]);
                                            $password = test_input(@$_POST["password_login"]);
                                            if ($email == "" || $password == "") {
                                                echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Le champs identifiant et mot de passe ne doivent pas rester vide </div>";
                                            } else {
                                                $password = sha1($password);
                                                require("php/bdd.php");
                                                $str = "SELECT * FROM user WHERE user_logname ='" . $email . "' AND user_motdepasse = '" . $password . "'";
                                                $result = bdd($str);
                                                if ($result) {
                                                    if ($result->num_rows > 0) {                //username and password correct         
                                                        while ($row = $result->fetch_array()) {

                                                            $_SESSION["admin"] = true;
                                                            $_SESSION["code"] = mt_rand(0, 100000);
                                                            $_SESSION["username"] = $row["user_name"];
                                                            $_SESSION["type"] = $row["user_type"];
                                                            $_SESSION["userid"] = $row["user_id"];
                                                            $_SESSION["teamid"] = $row["user_equipe_id"];
                                                            $username = $row["user_name"];
                                                            $userid = $_SESSION["userid"];
                                                            $str = "UPDATE user set user_time=current_timestamp() WHERE user_id=" . $userid;
                                                            bdd($str);
                                                            if ($_SESSION["type"] == '1') {
                                                                echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL= equipier.php'>";
                                                                exit;
                                                            } else {
                                                                if ($_SESSION["teamid"] != "") {
                                                                    $str = "SELECT application_id, application_user_id,user_name FROM application INNER JOIN user ON application_user_id = user_id WHERE application_team_id = " . $_SESSION['teamid'];
                                                                    $result = bdd($str);
                                                                    if ($result) {
                                                                        if ($result->num_rows > 0) {
                                                                            $_SESSION['demandes'] = array();
                                                                            $_SESSION['demande'] = $result->num_rows;
                                                                            while ($row = $result->fetch_array()) {
                                                                                array_push($_SESSION['demandes'], $row);
                                                                            }
                                                                        } else {
                                                                            $_SESSION['demande'] = 0;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $_SESSION['demande'] = 0;
                                                                }
                                                                echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL= chef.php'>";
                                                                exit;
                                                            }
                                                        }
                                                    } elseif ($result->num_rows == 0) { //username or password incorrect
                                                        echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>L'identifiant ou le mot de passe ne sont pas correctes.</div>";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <!-- Connection END --> 
                        </div> <!-- #tab-content1 -->
                        <div id="tab-content2" class="tab-content">
                            <!--  Inscription  -->       
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3> Inscription </h3>
                                </div>
                                <div class="panel-body">
                                    <form action = "index.php" method="post">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Identifiant </h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="email_ins_div">
                                                    <input type="email" class="form-control"  name = "email" placeholder="Email" >
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Nom et prénom </h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="name_ins_div">
                                                    <input  class="form-control"  name="name" placeholder="Nom et prenom">
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Mot de passe </h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="password1_ins_div">
                                                    <input name="password1" type="password" class="form-control" placeholder="Password" >
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Vérifier </h5>
                                                </div>
                                                <div class=" text-center col-lg-7" id="password2_ins_div">
                                                    <input name="password2" type="password" class="form-control"  placeholder="Password">
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center col-lg-4 ">
                                                    <h5> Type </h5>
                                                </div>
                                                <div class="text-left col-lg-7">

                                                    <select name = "type">
                                                        <option  value="1">Equipier</option>
                                                        <option  value="2">Chef d'équipe</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row" id="error_ins"> </div>
                                            <hr>
                                            <br/>
                                            <div class="row">
                                                <div class=" text-center">
                                                    <input type="Submit" class="btn btn-primary" name="inscription" value="Valider"/>
                                                </div>
                                            </div>

                                        </div>
                                        <?php
                                        if (@$_REQUEST['inscription']) {
                                            require("php/bdd.php");
                                            $email = $password = $result = $userid = $username = $usertype = "";

                                            function test_input($data) {
                                                $data = trim($data);
                                                $data = stripslashes($data);
                                                $data = htmlspecialchars($data);
                                                return $data;
                                            }

                                            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                                $email = test_input(@$_POST["email"]);
                                                $password1 = test_input(@$_POST["password1"]);
                                                $password2 = test_input(@$_POST["password2"]);
                                                $username = test_input(@$_POST["name"]);
                                                $usertype = test_input(@$_POST["type"]);
                                                if ($email == "" || $password1 == "" || $password2 == "" || $username == "" || $usertype == "") {
                                                    echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Remplir les champs libres</div>";
                                                    return;
                                                } else if ($password1 != $password2) {
                                                    echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Les 2 mots de passes ne sont pas identique</div>";
                                                    return;
                                                }
                                                $str = "SELECT user_id FROM user WHERE user_logname ='" . $email . "'";
                                                $result = bdd($str);
                                                if ($result) {
                                                    if ($result->num_rows > 0) {    //if user name existe        						
                                                        echo "<br/><div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Attention!</strong>Cette adresse mail est déjà utilisé</div>";
                                                        return;
                                                    } elseif ($result->num_rows == 0) { //if user name not existe
                                                        $password1 = sha1($password1);
                                                        $str = "INSERT INTO user(user_name,user_motdepasse,user_type,user_logname) VALUES('" . $username . "','" . $password1 . "','" . $usertype . "','" . $email . "')";
                                                        $result = bdd($str);
                                                        echo "<br/><div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Le compte a bien était créé</div>";
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </form>
                                </div>
                            </div>
                            <!-- Inscription END -->         
                        </div> 
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <?php
        require('php/repeat/footer.php');
        ?>
