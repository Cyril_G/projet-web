<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap - Prebuilt Layout</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-center">KAGGLE ESIGELEC</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <hr>
            <div class="row">
                <div class="text-center col-md-12">
                    <div class="panel panel-default"> 
                        <!-- Default panel contents -->
                        <div class="panel-heading">Liste des équipes</div>

                        <!-- table -->
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>Nom de l'équipe</th>
                                    <th>Chef d'équipe</th>
                                    <th>Description</th>
                                    <th>Date de création</th>
                                </tr>
                            </thead>
                            <?php
                            require("php/bdd.php");
                            $str = "SELECT team.team_id,team.team_name,user.user_name,team.team_discription,team.team_time FROM team INNER JOIN user ON team.team_chef_id=user.user_id ";
                            $result = bdd($str);
                            if ($result) {
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_array()) {
                                        echo "<tbody>";
                                        echo "<tr>";
                                        echo "<th>";
                                        echo $row["team_id"];
                                        echo "</th>";
                                        echo "<th>";
                                        echo $row["team_name"];
                                        echo "</th>";
                                        echo "<th>";
                                        echo $row["user_name"];
                                        echo "</th>";
                                        echo "<th>";
                                        echo $row["team_discription"];
                                        echo "</th>";
                                        echo "<th>";
                                        echo $row["team_time"];
                                        echo "</th>";
                                        echo "</tr>";
                                        echo "</tbody>";
                                    }
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <?php
            require("php/repeat/footer.php");
            ?>
            <hr>
        </div>
        <!-- Include all compiled plugins (below), or include individual files as needed --> 

    </body>
</html>
